import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';


platformBrowserDynamic().bootstrapModule(AppModule)
//  .then(() => {
//	console.log("it is prod !");
//    if ('serviceWorker' in navigator && environment.production) {
//      navigator.serviceWorker.register('../dist/ngsw-worker.js')
//  }})
  .catch(err => console.error(err));
