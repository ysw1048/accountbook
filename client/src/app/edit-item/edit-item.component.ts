import { Component } from '@angular/core';
import { Category, EditItemDto, Item } from '../state/item/item.model';
import { Store } from '@ngrx/store';
import { AppState } from '../state/app.state';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { FormBuilder } from '@angular/forms';
import { selectAddItem } from '../state/item/item.selector';
import { Subscription, take, tap } from 'rxjs';
import { ItemActions } from '../state/item/item.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss'],
})
export class EditItemComponent {
  categorySubscription: Subscription | undefined;
  isMobile = false;
  categories: Category[] = [];
  editItemForm;
  isFormError = false;
  item: Item | undefined;

  constructor(
    private store: Store<AppState>,
    breakpointObserver: BreakpointObserver,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.editItemForm = this.formBuilder.group({
      category: '',
      name: '',
      price: '',
      isRepeated: 'none',
      date: new Date().toISOString().split('.')[0],
      itemType: '',
    });
    this.subscribeRespontiveWeb(breakpointObserver);

    this.getCategories(store);

    this.editItemSetOriginalValue();
  }

  subscribeRespontiveWeb( breakpointObserver: BreakpointObserver) {
    breakpointObserver
      .observe([Breakpoints.Medium, Breakpoints.Large])
      .subscribe((result) => {
        if (result.matches) {
          this.isMobile = false;
        } else {
          this.isMobile = true;
        }
      });
  }

  getCategories(store: Store<AppState>) {
    this.categorySubscription = store
    .select(selectAddItem)
    .pipe(
      take(2),
      tap((categories) => {
        this.categories = categories;
      })
    )
    .subscribe();
  }

  editItemSetOriginalValue() {
    this.item = (
      this.router.getCurrentNavigation()?.extras.state as {
        item: Item;
      }
    ).item;

    let isRepeated;
    if (this.item.monthly) isRepeated = 'monthly';
    else if (this.item.yearly) isRepeated = 'yearly';
    else isRepeated = 'none';

    this.editItemForm.setValue({
      category: this.item.category.name,
      name: this.item.name,
      price: Math.abs(this.item.price).toString(),
      date: this.item.transaction_date.split('.')[0],
      isRepeated: isRepeated,
      itemType: this.item.price > 0 ? 'income' : 'expense',
    });
  }

  ngOnInit(): void {
    this.store.dispatch(ItemActions.loadCategories());

    document.getElementById('selectedCategory')!.innerHTML = this.item!.category.name;
  }

  choseCategory(event: Event) {
    event.preventDefault();
    const element = event.target as HTMLInputElement;
    // Not sure here but sometimes button is not loaded proprely when click multiple times
    // so check if the right property is chose
    if (element.className != 'mat-mdc-button-touch-target') {
      const category = element.innerText;

      const button = document.getElementById('categories') as HTMLElement;
      const categories = button.parentElement;
      const selectedCategory = document.getElementById('selectedCategory');
      // check if user already selected a category
      if (categories?.hidden) {
        // already selected the category then display the categories list
        categories!.hidden = false;
        selectedCategory!.hidden = true;
      } else {
        categories!.hidden = true;
        selectedCategory!.innerText = category;
        selectedCategory!.hidden = false;

        // set directly category value in form cuz button doesn't work with formControlName
        this.editItemForm.setValue({
          ...this.editItemForm.getRawValue(),
          category,
        });
      }
    }
  }

  editItem(event: Event) {
    event.preventDefault();

    const formValues = this.editItemForm.getRawValue();
    if (
      formValues.name &&
      formValues.price &&
      formValues.date &&
      formValues.category
    ) {
      const payload: EditItemDto = {
        id: this.item!.id,
        accountId: this.item!.accountId,
        categoryId: this.item!.category.id,
        name: formValues.name!,
        price:
          formValues.itemType == 'expense'
            ? -Number(formValues.price)
            : Number(formValues.price),
        monthly: false,
        yearly: false,
        transactionDate: formValues.date!,
      };

      switch (formValues.isRepeated) {
        case 'none':
          break;
        case 'monthly':
          payload.monthly = true;
          break;
        case 'yearly':
          payload.yearly = true;
          break;
      }

      this.store.dispatch(ItemActions.editItem(payload));
    } else {
      this.isFormError = true;
    }
  }

  deleteItem(event: Event) {
    event.preventDefault();
    this.store.dispatch(ItemActions.deleteItem({ itemId: this.item!.id }));
  }

  ngOnDestroy() {
    if (this.categorySubscription)
      this.categorySubscription.unsubscribe();
  }
}
