import { Component, EventEmitter, HostBinding, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppState } from '../state/app.state';
import { Store } from '@ngrx/store';
import { ItemListActions } from '../state/item-list/item-list.actions';
import { AuthActions } from '../state/auth/auth.actions';
import { Router } from '@angular/router';
import { selectToken } from '../state/auth/auth.selector';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Output() changeTheme = new EventEmitter<Boolean>();
  toggleControl = new FormControl(false);
  datepickerControl = new FormControl();
  isMobile = false;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    breakpointObserver: BreakpointObserver
  ) {
    const month = (new Date().getMonth() + 1).toString().padStart(2, '0');
    const year = new Date().getFullYear();
    this.datepickerControl.setValue(year + '-' + month);

    this.subscribeRespontiveWeb(breakpointObserver);
  }

  subscribeRespontiveWeb(breakpointObserver: BreakpointObserver) {
    breakpointObserver
      .observe([Breakpoints.Medium, Breakpoints.Large])
      .subscribe((result) => {
        if (result.matches) {
          this.isMobile = false;
        } else {
          this.isMobile = true;
        }
      });
  }

  ngOnInit(): void {
    this.toggleControl.valueChanges.subscribe((darkMode) => {
      this.changeTheme.emit(darkMode!);
      // const darkClassName = 'darkMode';
      // this.className = darkMode ? darkClassName : '';
    });

    this.store
      .select(selectToken)
      .subscribe((token) =>
        this.store.dispatch(AuthActions.authenticate({ token }))
      );
  }

  setDate() {
    const date = this.datepickerControl.getRawValue();
    const year = date.split('-')[0];
    const month = date.split('-')[1];

    if (month <= 12 && year.length == 4) {
      this.store.dispatch(ItemListActions.setDate({ date: date }));
    }
  }

  changeDate(dir: string) {
    const date = this.datepickerControl.getRawValue();
    const year = date.split('-')[0];
    const month = date.split('-')[1];

    let newDate;
    if (dir == 'prev') {
      if (month == '01') {
        newDate = (Number(year) - 1).toString() + '-12';
      } else {
        newDate = year + '-' + (month - 1).toString().padStart(2, '0');
      }
    } else if (dir == 'next') {
      if (month == '12') {
        newDate = (Number(year) + 1).toString() + '-01';
      } else {
        newDate = year + '-' + (Number(month) + 1).toString().padStart(2, '0');
      }
    }

    this.datepickerControl.setValue(newDate);
    this.setDate();
  }

  logout() {
    this.store.dispatch(AuthActions.logOut());
    this.router.navigate(['./']);
  }
}
