import { DOCUMENT } from '@angular/common';
import { Component, HostBinding, Inject } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'client';

  @HostBinding('class') className = '';
  constructor(@Inject(DOCUMENT) private document: Document) {}

  onChangeTheme(darkMode: Boolean) {
    const darkClassName = 'darkMode';
    const lightClassName = 'lightMode';
    if (darkMode) {
      // this.className = darkClassName;
      this.document.body.classList.remove(lightClassName);
      this.document.body.classList.add(darkClassName);
    } else {
      // this.className = '';
      this.document.body.classList.remove(darkClassName);
      this.document.body.classList.add(lightClassName);
    }
  }
}
