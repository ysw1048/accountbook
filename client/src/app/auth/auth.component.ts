import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../state/app.state';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { selectId, selectToken } from '../state/auth/auth.selector';
import { AuthActions } from '../state/auth/auth.actions';
import { NonNullableFormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  isMobile = false;
  authForm;
  pwdHide = true;
  loginError = false;

  constructor(
    private store: Store<AppState>,
    private breakpointObserver: BreakpointObserver,
    private formBuilder: NonNullableFormBuilder
  ) {
    this.authForm = this.formBuilder.group({
      email: ['', Validators.email],
      password: '',
    });

    this.subscribeRespontiveWeb(breakpointObserver);
  }

  subscribeRespontiveWeb(breakpointObserver: BreakpointObserver) {
    breakpointObserver
    .observe([Breakpoints.Medium, Breakpoints.Large])
    .subscribe((result) => {
      if (result.matches) {
        this.isMobile = false;
      } else {
        this.isMobile = true;
      }
    });

  }

  ngOnInit(): void {
    this.store
      .select(selectToken)
      .subscribe((token: string) =>
        this.store.dispatch(AuthActions.authenticate({ token }))
      );
  }

  auth(event: Event) {
    event.preventDefault();

    if (!this.authForm.get('email')?.hasError('email')) {
      const formValues = this.authForm.getRawValue();
      // if empty, display alart
      const payload = {
        email: formValues.email,
        password: formValues.password,
      };

      this.store.dispatch(AuthActions.login(payload));
      this.store.select(selectId).subscribe((id) => {
        console.log(id);
        this.loginError = id == -1 ? true : false;
      });
    }
  }

  guestAuth(event: Event) {
    event.preventDefault();
    const payload = {
      email: 'guest@guest.com',
      password: 'guest',
    };

    this.store.dispatch(AuthActions.login(payload));
  }

  checkInputsSet() {
    const formValues = this.authForm.getRawValue();
    const loginButton = document.getElementById(
      'loginButton'
    ) as HTMLInputElement;

    if (
      formValues.email &&
      formValues.password &&
      !this.authForm.get('email')?.hasError('email')
    ) {
      loginButton.disabled = false;
    } else {
      loginButton.disabled = true;
    }
  }
}
