import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../state/app.state';
import { ItemListActions } from '../state/item-list/item-list.actions';
import { selectItemList } from '../state/item-list/item-list.selector';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { NGXLogger } from 'ngx-logger';
import { Item } from '../state/item/item.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit {
  itemSubscription;
  items: { [day: string]: Item[] } = {};
  isMobile = false;
  income = 0;
  expense = 0;
  sum = '';

  constructor(
    private store: Store<AppState>,
    breakpointObserver: BreakpointObserver,
    private router: Router
  ) {
    this.itemSubscription = this.store
      .select(selectItemList)
      .pipe()
      .subscribe((itemList) => {
        let income = 0;
        let expense = 0;
        [income, expense] = this.getItemsByMonthYear(itemList);

        this.orderItemsByDate();
        
        this.calculSumOfIncomeExpenseTotal(income, expense)
      });

      this.subscribeRespontiveWeb(breakpointObserver);
  }

  subscribeRespontiveWeb(breakpointObserver: BreakpointObserver): void {
    breakpointObserver
      .observe([Breakpoints.Medium, Breakpoints.Large])
      .subscribe((result) => {
        if (result.matches) {
          this.isMobile = false;
        } else {
          this.isMobile = true;
        }
      });
  }

  getItemsByMonthYear(itemList: { items: any[]; year: number; month: number; }) {
    this.items = {};
    let income = 0;
    let expense = 0;

    const filterdItems = itemList.items.filter((item) => {
      const itemYear = Number(item.transaction_date.split('-')[0]);
      const itemMonth = Number(item.transaction_date.split('-')[1]);
      return itemList.year == itemYear && itemList.month == itemMonth;
    });

    filterdItems.map((item) => {
      const day: string = new Date(
        item.transaction_date
      ).toLocaleDateString();

      if (!(day in this.items)) {
        this.items[day] = [];
      }

      // refresh data up to date
      this.items[day] = this.items[day].filter(
        (_item) => _item.id !== item.id
      );
      this.items[day].push(item);

      // calcul sum
      item.price > 0 ? (income += item.price) : (expense += item.price);
    });

    return [income, expense]
  }

  orderItemsByDate() {
    Object.keys(this.items).forEach((day) => {
      this.items[day].sort((a, b) => {
        return new Date(a.transaction_date) > new Date(b.transaction_date)
          ? 1
          : -1;
      });
    });

    this.items = Object.keys(this.items)
      .sort((a, b) => {
        return new Date(a) > new Date(b) ? 1 : -1;
      })
      .reduce((r, k) => {
        r[k] = this.items[k];
        return r;
      }, {} as { [day: string]: Item[] });
  }

  calculSumOfIncomeExpenseTotal(income: number, expense: number) {
    this.income = Number(income.toFixed(2));
    this.expense = Number(expense.toFixed(2));
    this.sum = (income + expense).toFixed(2);
  }

  ngOnInit() {
    this.store.dispatch(ItemListActions.loadItems());
  }

  editItem(item: Item) {
    console.log(item);
    this.router.navigate(['./edititem'], { state: { item } });
  }

  orderDay() {
    return 0;
  }

  ngOnDestroy() {
    this.itemSubscription.unsubscribe();
    console.log('unsubscription');
  }
}
