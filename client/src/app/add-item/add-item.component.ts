import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../state/app.state';
import { ItemActions } from '../state/item/item.actions';
import { Category, AddItemDto } from '../state/item/item.model';
import { selectAddItem } from '../state/item/item.selector';
import { Subscription, take, tap } from 'rxjs';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss'],
})
export class AddItemComponent implements OnInit {
  categorySubscription: Subscription | undefined;
  isMobile = false;
  categories: Category[] = [];
  addItemForm;
  isFormError = false;
  constructor(
    private store: Store<AppState>,
    private breakpointObserver: BreakpointObserver,
    private formBuilder: FormBuilder
  ) {
    this.addItemForm = this.formBuilder.group({
      category: '',
      name: '',
      price: '',
      isRepeated: 'none',
      date: new Date().toISOString().split('.')[0],
      itemType: 'expense',
    });

    this.subscribeRespontiveWeb(breakpointObserver);    

    this.getCategories(store);
  }

  subscribeRespontiveWeb(breakpointObserver: BreakpointObserver): void {
    breakpointObserver
      .observe([Breakpoints.Medium, Breakpoints.Large])
      .subscribe((result) => {
        if (result.matches) {
          this.isMobile = false;
        } else {
          this.isMobile = true;
        }
      });
  }

  getCategories(store: Store<AppState>) {
    this.categorySubscription = store
      .select(selectAddItem)
      .pipe(
        take(2),
        tap((categories) => {
          this.categories = categories;
        })
      )
      .subscribe();
  }
  

  ngOnInit(): void {
    this.store.dispatch(ItemActions.loadCategories());
  }

  choseCategory(event: Event) {
    event.preventDefault();
    const element = event.target as HTMLInputElement;
    // Not sure here but sometimes button is not loaded proprely when click multiple times
    // so check if the right property is chose
    if (element.className != 'mat-mdc-button-touch-target') {
      const category = element.innerText;

      const button = document.getElementById('categories') as HTMLElement;
      const categories = button.parentElement;
      const selectedCategory = document.getElementById('selectedCategory');
      // check if user already selected a category
      if (categories?.hidden) {
        // already selected the category then display the categories list
        categories!.hidden = false;
        selectedCategory!.hidden = true;
      } else {
        categories!.hidden = true;
        selectedCategory!.innerText = category;
        selectedCategory!.hidden = false;

        // set directly category value in form cuz button doesn't work with formControlName
        this.addItemForm.setValue({
          ...this.addItemForm.getRawValue(),
          category,
        });
      }
    }
  }

  addItem(event: Event) {
    event.preventDefault();
    const formValues = this.addItemForm.getRawValue();

    if (
      formValues.name &&
      formValues.price &&
      formValues.date &&
      formValues.category
    ) {
      const categoryId = this.categories.filter(
        (category) => category.name === formValues.category
      )[0].id;

      const payload: AddItemDto = {
        category: {
          id: categoryId,
          name: formValues.category!,
        },
        name: formValues.name!,
        price:
          formValues.itemType == 'expense'
            ? -Number(formValues.price)
            : Number(formValues.price),
        monthly: false,
        yearly: false,
        transactionDate: formValues.date!,
      };

      switch (formValues.isRepeated) {
        case 'none':
          break;
        case 'monthly':
          payload.monthly = true;
          break;
        case 'yearly':
          payload.yearly = true;
          break;
      }

      this.store.dispatch(ItemActions.addItem(payload));
    } else {
      this.isFormError = true;
    }
  }

  ngOnDestroy() {
    if (this.categorySubscription)
      this.categorySubscription.unsubscribe();
  }
}
