// import { createSelector } from '@ngrx/store';
// import { ItemList } from './item-list.model';
import { AppState } from '../app.state';

export const selectItemList = (state: AppState) => state.itemList;
// export const selectItems = createSelector(
//   selectItemList,
//   (state: ItemList) => state.items,
// );
