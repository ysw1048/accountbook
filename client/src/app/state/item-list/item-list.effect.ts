import { Injectable } from '@angular/core';
import { ItemListService } from './item-list.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ItemListActions } from './item-list.actions';
import { catchError, exhaustMap, map, of, tap } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';

@Injectable()
export class ItemListEffects {
  constructor(
    private actions$: Actions,
    private itemListSerivce: ItemListService,
    private logger: NGXLogger,
    private router: Router
  ) {}

  loadItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ItemListActions.loadItems),
      exhaustMap(() =>
        this.itemListSerivce.loadItems().pipe(
          tap(() => this.logger.info('load item effect called')),
          map((response) => {
            return ItemListActions.loadItemsSuccess({
              items: Object.values(response),
            });
          }),
          catchError((err) => {
            this.logger.error(err);
            if (err.state == 401) this.router.navigate(['./']);
            return of(ItemListActions.loadItemsError(err));
          })
        )
      )
    )
  );
}
