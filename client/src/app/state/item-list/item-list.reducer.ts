import { createReducer, on } from '@ngrx/store';
import { ItemListActions } from './item-list.actions';
import { ItemList } from './item-list.model';
import { inject } from '@angular/core';
import { NGXLogger } from 'ngx-logger';

export const initialState: ItemList = {
  userId: 0,
  items: [],
  year: new Date().getFullYear(),
  month: new Date().getMonth() + 1,
};

export const ItemListReducer = createReducer(
  initialState,
  on(ItemListActions.loadItemsSuccess, (state, data) => {
    const items = structuredClone(data.items).map((item) => {
      return item;
    });
    return {
      ...state,
      items: items,
    };
  }),
  on(ItemListActions.loadItemsError, (state, errorMsg) => {
    // to remove if no treatment needed
    console.log(errorMsg);
    return state;
  }),
  on(ItemListActions.setDate, (state, payload) => {
    const year = Number(payload.date.split('-')[0]);
    const month = Number(payload.date.split('-')[1]);

    return {
      ...state,
      year,
      month,
    };
  })
);
