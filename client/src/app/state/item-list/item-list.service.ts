import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import { selectToken } from '../auth/auth.selector';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ItemListService {
  constructor(
    private http: HttpClient,
    private logger: NGXLogger,
    private store: Store<AppState>
  ) {}

  loadItems() {
    this.logger.info('getting items in ItemListService!!');
    let token;
    this.store.select(selectToken).subscribe((t) => {
      token = t;
    });
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });
    const options = { headers };
    return this.http
      .get<Response>(`${environment.apiUrl}/items?userid=1`, options)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }
}
