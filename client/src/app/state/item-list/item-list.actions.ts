import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Item } from '../item/item.model';

export const ItemListActions = createActionGroup({
  source: 'ItemList',
  events: {
    'Load Items': emptyProps(),
    'Load Items Success': props<{ items: Item[] }>(),
    'Load Items Error': props<{ errorMsg: string }>(),
    'Set Date': props<{ date: string }>(),
  },
});
