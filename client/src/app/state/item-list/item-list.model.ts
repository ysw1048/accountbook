import { Item } from '../item/item.model';

export interface ItemList {
  userId: number;
  items: Item[];
  year: number;
  month: number;
}
