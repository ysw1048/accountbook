import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { AddItemDto, Category, EditItemDto, Item } from './item.model';

export const ItemActions = createActionGroup({
  source: 'AddItem',
  events: {
    'Load Categories': emptyProps(),
    'Load Categories Success': props<{ categories: Category[] }>(),
    'Load Categories Error': emptyProps(),
    'Add Item': props<AddItemDto>(),
    'Add Item Success': emptyProps(),
    'Add Item Error': emptyProps(),
    'Edit Item': props<EditItemDto>(),
    'Edit Item Success': emptyProps(),
    'Edit Item Error': emptyProps(),
    'Delete Item': props<{ itemId: number }>(),
    'Delete Item Success': emptyProps(),
    'Delete Item Error': emptyProps(),
  },
});
