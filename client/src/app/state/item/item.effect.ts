import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NGXLogger } from 'ngx-logger';
import { catchError, exhaustMap, last, map, of, tap } from 'rxjs';
import { ItemActions } from './item.actions';
import { Router } from '@angular/router';
import { ItemService } from './item.service';

@Injectable()
export class ItemEffects {
  constructor(
    private actions$: Actions,
    private ItemService: ItemService,
    private logger: NGXLogger,
    private router: Router
  ) {}

  loadCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ItemActions.loadCategories),
      exhaustMap(() =>
        this.ItemService.loadCategories().pipe(
          map((response) => {
            return ItemActions.loadCategoriesSuccess({
              categories: Object.values(response),
            });
          }),
          catchError((err) => {
            this.logger.error(err);
            return of(ItemActions.loadCategoriesError());
          })
        )
      )
    )
  );

  addItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ItemActions.addItem),
      exhaustMap((ItemDto) =>
        this.ItemService.addItem(ItemDto).pipe(
          map(() => {
            return ItemActions.addItemSuccess();
          }),
          tap(() => this.router.navigate(['./itemList'])),
          catchError((err) => {
            this.logger.error(err);
            if (err.state == 403) this.router.navigate(['./']);
            return of(ItemActions.loadCategoriesError());
          })
        )
      )
    )
  );

  editItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ItemActions.editItem),
      exhaustMap((item) =>
        this.ItemService.editItem(item).pipe(
          map(() => ItemActions.editItemSuccess()),
          tap(() => this.router.navigate(['./itemList'])),
          catchError((err) => {
            this.logger.error(err);
            if (err.state == 403) this.router.navigate(['./']);
            return of(ItemActions.loadCategoriesError());
          })
        )
      )
    )
  );

  deleteItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ItemActions.deleteItem),
      exhaustMap((payload: { itemId: number }) =>
        this.ItemService.deleteItem(payload.itemId).pipe(
          map(() => ItemActions.deleteItemSuccess()),
          tap(() => this.router.navigate(['./itemList'])),
          catchError((err) => {
            this.logger.error(err);
            if (err.state == 403) this.router.navigate(['./']);
            return of(ItemActions.loadCategoriesError());
          })
        )
      )
    )
  );
}
