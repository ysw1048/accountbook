import { createReducer, on } from '@ngrx/store';
import { Category } from './item.model';
import { ItemActions } from './item.actions';

const initialState: Category[] = [];

export const ItemReducer = createReducer(
  initialState,
  on(ItemActions.loadCategoriesSuccess, (state, data) => {
    const categoires = state
      .concat(data.categories)
      .filter((category) => state.indexOf(category) < 0);
    return categoires;
  }),
  on(ItemActions.loadCategoriesError, (state) => {
    return state;
  }),
  on(ItemActions.addItemSuccess, (state) => state),
  on(ItemActions.addItemError, (state) => state),
  on(ItemActions.editItemSuccess, (state) => state),
  on(ItemActions.editItemError, (state) => state),
  on(ItemActions.deleteItemSuccess, (state) => state),
  on(ItemActions.deleteItemError, (state) => state)
);
