import { AppState } from '../app.state';

export const selectAddItem = (state: AppState) => state.addItem;
