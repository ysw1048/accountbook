interface Translation {
  id: number;
  en: string;
  fr: string;
  kr: string;
}

export interface Category {
  id: number;
  name: string;
  isCustom: boolean;
  account: number | null;
  translation: Translation;
}

export interface Item {
  id: number;
  name: string;
  price: number;
  transaction_date: string;
  accountId: number;
  category: Category;
  monthly: boolean;
  yearly: boolean;
}

export interface AddItemDto {
  category: {
    id: number;
    name: string;
  };
  name: string;
  price: number;
  monthly: boolean;
  yearly: boolean;
  transactionDate: string;
}

export interface EditItemDto {
  id: number;
  accountId: number;
  categoryId: number;
  name: string;
  price: number;
  monthly: boolean;
  yearly: boolean;
  transactionDate: string;
}
