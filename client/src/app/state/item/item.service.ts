import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { map } from 'rxjs';
import { AddItemDto, EditItemDto, Item } from './item.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import { selectId, selectToken } from '../auth/auth.selector';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  constructor(
    private http: HttpClient,
    private logger: NGXLogger,
    private store: Store<AppState>
  ) {}

  loadCategories() {
    this.logger.info('getting categories in ItemService');
    let token;
    this.store.select(selectToken).subscribe((t) => {
      token = t;
    });
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });
    const options = { headers };
    return this.http
      .get<Response>(`${environment.apiUrl}/categories`, options)
      .pipe(
        map((response: Response) => {
          this.logger.debug(response);
          return response;
        })
      );
  }

  addItem(dto: AddItemDto) {
    this.logger.info('add item in ItemService');
    let accountId;
    this.store
      .select(selectId)
      .subscribe((id) => (accountId = id))
      .unsubscribe();
    let token;
    this.store
      .select(selectToken)
      .subscribe((t) => (token = t))
      .unsubscribe();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });
    const options = { headers };
    const body = {
      name: dto.name,
      price: dto.price,
      accountId: accountId,
      categoryId: dto.category.id,
      monthly: dto.monthly,
      yearly: dto.yearly,
      transactionDate: dto.transactionDate,
    };
    return this.http
      .post<Response>(`${environment.apiUrl}/items`, body, options)
      .pipe();
  }

  editItem(dto: EditItemDto) {
    this.logger.info('edit item in ItemService');
    let accountId;
    this.store
      .select(selectId)
      .subscribe((id) => (accountId = id))
      .unsubscribe();
    let token;
    this.store
      .select(selectToken)
      .subscribe((t) => (token = t))
      .unsubscribe();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });
    const options = { headers };

    const body = {
      id: dto.id,
      name: dto.name,
      price: dto.price,
      accountId: accountId,
      categoryId: dto.categoryId,
      monthly: dto.monthly,
      yearly: dto.yearly,
      transactionDate: dto.transactionDate,
    };
    return this.http
      .put<Response>(`${environment.apiUrl}/items`, body, options)
      .pipe();
  }

  deleteItem(itemId: number) {
    this.logger.info('delete item in ItemService');
    let accountId;
    this.store
      .select(selectId)
      .subscribe((id) => (accountId = id))
      .unsubscribe();
    let token;
    this.store
      .select(selectToken)
      .subscribe((t) => (token = t))
      .unsubscribe();
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });
    const options = { headers, params: { itemId } };

    return this.http
      .delete<Response>(`${environment.apiUrl}/items`, options)
      .pipe();
  }
}
