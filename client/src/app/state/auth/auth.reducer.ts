import { createReducer, on } from '@ngrx/store';
import { AuthActions } from './auth.actions';
import { Account } from './auth.model';
import { inject } from '@angular/core';
import { NGXLogger } from 'ngx-logger';

const initialState: Account = {
  id: -1,
  name: '',
  email: '',
  token: '',
};

export const AuthReducer = createReducer(
  initialState,
  on(AuthActions.loginSuccess, (state, data) => {
    return data;
  }),
  on(AuthActions.loginError, (state, err) => {
    console.log(err);
    return state;
  }),
  on(AuthActions.authenticateSuccess, (state) => {
    return state;
  }),
  on(AuthActions.authenticateError, (state, err) => {
    console.log(err);
    return state;
  }),
  on(AuthActions.logOut, () => {
    return initialState;
  })
);
