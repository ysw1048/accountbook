import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Account } from './auth.model';

export const AuthActions = createActionGroup({
  source: 'Auth',
  events: {
    Login: props<{ email: string; password: string }>(),
    'Login Success': props<Account>(),
    'Login Error': props<{ errorMsg: string }>(),
    Authenticate: props<{ token: string }>(),
    'Authenticate Success': emptyProps(),
    'Authenticate Error': props<{ errorMsg: string }>(),
    'Log out': emptyProps(),
  },
});
