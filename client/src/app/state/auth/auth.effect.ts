import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NGXLogger } from 'ngx-logger';
import { AuthActions } from './auth.actions';
import { catchError, exhaustMap, from, map, of, tap } from 'rxjs';
import { Account } from './auth.model';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private logger: NGXLogger,
    private router: Router
  ) {}

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      exhaustMap((login: { email: string; password: string }) =>
        this.authService.login(login.email, login.password).pipe(
          map((data) => AuthActions.loginSuccess(data)),
          tap(() => this.router.navigate(['./itemList'])),
          catchError((err) => of(AuthActions.loginError(err)))
        )
      )
    )
  );

  authenticate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.authenticate),
      exhaustMap((payload: { token: string }) =>
        this.authService.authenticate(payload.token).pipe(
          map(() => AuthActions.authenticateSuccess()),
          tap(() => this.router.navigate(['./itemList'])),
          catchError((err) => {
            this.router.navigate(['./']);
            return of(AuthActions.authenticateError(err));
          })
        )
      )
    )
  );
}
