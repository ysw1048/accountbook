import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { Account } from './auth.model';

export const selectAccount = (state: AppState) => state.account;
export const selectId = createSelector(
  selectAccount,
  (state: Account) => state.id
);
export const selectToken = createSelector(
  selectAccount,
  (state: Account) => state.token
);
