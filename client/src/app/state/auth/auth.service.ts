import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Account } from './auth.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private logger: NGXLogger) {}

  login(email: string, password: string) {
    this.logger.info('logging in in AuthService');
    this.logger.info(`${environment.apiUrl}/accounts/login`);
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    const body = {
      email,
      password,
    };
    return this.http.post<Account>(
      `${environment.apiUrl}/accounts/login`,
      body,
      options
    );
  }

  authenticate(token: string) {
    this.logger.info('authenticating in AuthService');
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    const body = { token };
    return this.http.post<String>(
      `${environment.apiUrl}/accounts/authenticate`,
      body,
      options
    );
  }
}
