import { inject } from '@angular/core';
import { AuthService } from './auth.service';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import { selectToken } from './auth.selector';

export const authGuard = () => {
  const authService = inject(AuthService);
  const store = inject(Store<AppState>);

  let token = '';
  store.select(selectToken).subscribe((t) => (token = t));

  return authService.authenticate(token);
};
