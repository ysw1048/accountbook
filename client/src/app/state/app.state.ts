import { ActionReducerMap } from '@ngrx/store';
import { ItemList } from './item-list/item-list.model';
import { ItemListReducer } from './item-list/item-list.reducer';
import { Category } from './item/item.model';
import { ItemReducer } from './item/item.reducer';
import { AuthReducer } from './auth/auth.reducer';
import { Account } from './auth/auth.model';

export interface AppState {
  itemList: ItemList;
  addItem: Category[];
  account: Account;
}

export const appReducers: ActionReducerMap<AppState> = {
  itemList: ItemListReducer,
  addItem: ItemReducer,
  account: AuthReducer,
};
