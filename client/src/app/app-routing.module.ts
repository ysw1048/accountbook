import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemListComponent } from './item-list/item-list.component';
import { AddItemComponent } from './add-item/add-item.component';
import { AuthComponent } from './auth/auth.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { authGuard } from './state/auth/authguard.service';

const routes: Routes = [
  { path: '', component: AuthComponent },
  { path: 'itemList', component: ItemListComponent, canActivate: [authGuard] },
  { path: 'additem', component: AddItemComponent, canActivate: [authGuard] },
  { path: 'edititem', component: EditItemComponent, canActivate: [authGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
