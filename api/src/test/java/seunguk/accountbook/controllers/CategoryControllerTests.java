package seunguk.accountbook.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import seunguk.accountbook.TestHelper;
import seunguk.accountbook.models.Category;
import seunguk.accountbook.services.CategoryService;

@SpringBootTest
@AutoConfigureMockMvc
public class CategoryControllerTests {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CategoryService categoryService;

	@Autowired
	private TestHelper testHelper;

	@Test
	public void getAllCategories_success() throws Exception {
		// given
		Category category1 = Category.builder().name("category1").build();
		Category category2 = Category.builder().name("category2").build();

		List<Category> categories = Arrays.asList(new Category[] { category1, category2 });
		Mockito.when(categoryService.getAllCategories()).thenReturn(categories);

		testHelper.setToken("testuser@test.com");

		// when
		ResultActions response = mockMvc.perform(
				get("/categories")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON));

		// then
		response.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.size()", is(categories.size())));
	}

	@Test
	public void getAllCategories_withoutJwt() throws Exception {
		// given
		Category category1 = Category.builder().name("category1").build();
		Category category2 = Category.builder().name("category2").build();

		List<Category> categories = Arrays.asList(new Category[] { category1,
				category2 });
		Mockito.when(categoryService.getAllCategories()).thenReturn(categories);

		// when
		ResultActions response = mockMvc.perform(
				get("/categories")
						.contentType(MediaType.APPLICATION_JSON));

		// then
		response.andDo(print())
				.andExpect(status().isForbidden());
	}

	@Test
	public void addDefaultCategories_success() throws Exception {
		// given
		testHelper.setToken("testuser@test.com");

		// when
		ResultActions response = mockMvc.perform(
				put("/categories/default")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token"));

		// then
		response.andDo(print())
				.andExpect(status().isNoContent());
	}
}
