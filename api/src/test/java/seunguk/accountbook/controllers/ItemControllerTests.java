package seunguk.accountbook.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.ObjectMapper;

import seunguk.accountbook.TestHelper;
import seunguk.accountbook.controllers.dto.Item_addItemDto;
import seunguk.accountbook.controllers.dto.Item_editItemDto;
import seunguk.accountbook.controllers.dto.Item_getItemsDto;
import seunguk.accountbook.models.Item;
import seunguk.accountbook.services.ItemService;
import seunguk.accountbook.services.dto.ItemService_AddItemDto;

@SpringBootTest
@AutoConfigureMockMvc
public class ItemControllerTests {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private TestHelper testHelper;

	@MockBean
	private ItemService itemService;

	@Test
	public void getItems_success() throws Exception {
		// given
		Item_getItemsDto getItemDto1 = Item_getItemsDto.builder()
				.name("item1")
				.price(BigDecimal.valueOf(100))
				.build();
		Item_getItemsDto getItemDto2 = Item_getItemsDto.builder()
				.name("item2")
				.price(BigDecimal.valueOf(50))
				.build();
		List<Item_getItemsDto> items = Arrays.asList(new Item_getItemsDto[] { getItemDto1, getItemDto2 });

		Mockito.when(itemService.getUserItems(anyString()))
				.thenReturn(items);

		testHelper.setToken("testuser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				get("/items")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON));

		// then
		response.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.size()", is(items.size())));
	}

	@Test
	public void addItem_success() throws Exception {
		// given
		Item_addItemDto itemDto = Item_addItemDto.builder()
				.name("item")
				.price(BigDecimal.valueOf(100))
				.accountId(1)
				.categoryId(1)
				.monthly(false)
				.yearly(false)
				.transactionDate(new Timestamp(0))
				.build();

		testHelper.setToken("testuser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				post("/items")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(itemDto)));

		// then
		response.andDo(print())
				.andExpect(status().isCreated());

	}

	@Test
	public void addItem_missingField() throws Exception {
		// given
		Item_addItemDto itemDto = Item_addItemDto.builder()
				.name("item")
				.price(BigDecimal.valueOf(100))
				.monthly(false)
				.yearly(false)
				.transactionDate(new Timestamp(0))
				.build();

		testHelper.setToken("testuser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				post("/items")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(itemDto)));

		// then
		response.andDo(print())
				.andExpect(status().isUnprocessableEntity());

	}

	@Test
	public void addItem_notItsOwnItem() throws Exception {
		// given
		Item_addItemDto itemDto = Item_addItemDto.builder()
				.name("item")
				.price(BigDecimal.valueOf(100))
				.accountId(1)
				.categoryId(1)
				.monthly(false)
				.yearly(false)
				.transactionDate(new Timestamp(System.currentTimeMillis()))
				.build();

		ItemService_AddItemDto itemService_AddItemDto = ItemService_AddItemDto.builder()
												.name("name")
												.price(BigDecimal.valueOf(100))
												.accountId(2)
												.categoryId(1)
												.monthly(false)
												.yearly(false)
												.transactionDate(new Timestamp(System.currentTimeMillis()))
												.build();

		Mockito.doThrow(ResponseStatusException.class)
				.when(itemService)
				.addItem(itemService_AddItemDto);

		testHelper.setToken("testuser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				post("/items")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(itemDto)));

		// then
		response.andDo(print())
				.andExpect(status().isCreated());

	}

	@Test
	public void editItem_success() throws Exception {
		// given
		Item_editItemDto itemDto = Item_editItemDto.builder()
				.id(1)
				.name("item")
				.accountId(1)
				.build();
		Item retureditem = Item.builder().id(1).name("item").build();

		Mockito.when(itemService.editItem("test@test.com", itemDto))
			.thenReturn(retureditem);

		testHelper.setToken("testuser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				put("/items")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(itemDto)));

		// then
		response.andDo(print())
				.andExpect(status().isNoContent());
	}

	@Test
	public void editItem_missingMustField() throws Exception {
		// given
		Item_editItemDto itemDto = Item_editItemDto.builder()
				.id(1)
				.name("item")
				.build();

		Mockito.doThrow(ResponseStatusException.class)
				.when(itemService)
				.editItem("test@test.com", itemDto);

		testHelper.setToken("testuser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				put("/items")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(itemDto)));

		// then
		response.andDo(print())
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void deleteItem_success() throws Exception {
		// given
		Mockito.doNothing()
				.when(itemService)
				.deleteItem(eq("test@test.com"), anyInt());

		testHelper.setToken("testuser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				delete("/items?itemId=1")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON));

		// then
		response.andDo(print())
				.andExpect(status().isNoContent());

	}

	@Test
	public void deleteItem_notItsOwnItem() throws Exception {
		// given
		Mockito.doThrow(new ResponseStatusException(HttpStatus.UNAUTHORIZED))
				.when(itemService)
				.deleteItem(not(eq("test@test.com")), eq(1));

		testHelper.setToken("wronguser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				delete("/items")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON)
						.queryParam("itemId", "1"));

		// then
		response.andDo(print())
				.andExpect(status().isUnauthorized());

	}

	@Test
	public void deleteItem_itemNotExist() throws Exception {
		// given
		Mockito.doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND))
				.when(itemService)
				.deleteItem(eq("testuser@test.com"), eq(1));

		testHelper.setToken("testuser@test.com");
		// when
		ResultActions response = mockMvc.perform(
				delete("/items")
						.header(HttpHeaders.AUTHORIZATION, "Bearer token")
						.contentType(MediaType.APPLICATION_JSON)
						.queryParam("itemId", "1"));

		// then
		response.andDo(print())
				.andExpect(status().isNotFound());
	}
}
