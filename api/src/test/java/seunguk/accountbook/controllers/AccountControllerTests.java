package seunguk.accountbook.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.ObjectMapper;

import seunguk.accountbook.controllers.dto.AccountRequestDto;
import seunguk.accountbook.controllers.dto.AccountResponseDto;
import seunguk.accountbook.controllers.dto.AuthenticateRequestDto;
import seunguk.accountbook.controllers.dto.LoginRequestDto;
import seunguk.accountbook.services.AccountService;

@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTests {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AccountService accountService;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void addAccount_seccess() throws Exception {
		// given
		String name = "testuser";
		String email = "testuser@test.com";
		String password = "testpwd";
		String token = "testToken";

		AccountRequestDto accountRequestDto = AccountRequestDto.builder()
				.name(name)
				.email(email)
				.password(password)
				.build();

		AccountResponseDto accountResponseDto = AccountResponseDto.builder()
				.name(name)
				.email(email)
				.token(token)
				.build();

		Mockito.when(accountService.addAccount(name, email, password))
				.thenReturn(accountResponseDto);

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(accountRequestDto)));

		// then
		response.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.name", is(accountResponseDto.getName())))
				.andExpect(jsonPath("$.email", is(accountResponseDto.getEmail())))
				.andExpect(jsonPath("$.token", is(accountResponseDto.getToken())));
	}

	@Test
	public void addAccount_missingField() throws Exception {
		// given
		String email = "testuser@test.com";
		String password = "testpwd";

		AccountRequestDto accountRequestDto = AccountRequestDto.builder()
				.email(email)
				.password(password)
				.build();

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(accountRequestDto)));

		// then
		response.andDo(print())
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void addAccount_missingPassword() throws Exception {
		// given
		String name = "testuser";
		String email = "testuser@test.com";

		AccountRequestDto accountRequestDto = AccountRequestDto.builder()
				.email(email)
				.name(name)
				.build();

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(accountRequestDto)));

		// then
		response.andDo(print())
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void login_seccess() throws Exception {
		// given
		String name = "testuser";
		String email = "testuser@test.com";
		String password = "testpwd";
		String token = "testToken";

		LoginRequestDto loginDto = LoginRequestDto.builder()
				.email(email)
				.password(password)
				.build();

		AccountResponseDto accountResponseDto = AccountResponseDto.builder()
				.name(name)
				.email(email)
				.token(token)
				.build();

		Mockito.when(accountService.login(loginDto.getEmail(), loginDto.getPassword()))
				.thenReturn(accountResponseDto);

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts/login")
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(loginDto)));

		// then
		response.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(name)))
				.andExpect(jsonPath("$.email", is(email)))
				.andExpect(jsonPath("$.token", is(token)));
	}

	@Test
	public void login_wrongPassword() throws Exception {
		// given
		String name = "testuser";
		String email = "testuser@test.com";
		String password = "wrongpwd";

		AccountRequestDto accountRequestDto = AccountRequestDto.builder()
				.name(name)
				.email(email)
				.password(password)
				.build();

		Mockito.when(accountService.login(email, password))
				.thenThrow(new BadCredentialsException(""));

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts/login")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(accountRequestDto)));

		// then
		response.andDo(print())
				.andExpect(status().isUnauthorized());
	}

	@Test
	public void login_wrongEmail() throws Exception {
		// given
		String name = "testuser";
		String wrongEmail = "wronguser@test.com";
		String password = "pwd";

		AccountRequestDto accountRequestDto = AccountRequestDto.builder()
				.name(name)
				.email(wrongEmail)
				.password(password)
				.build();

		Mockito.when(accountService.login(wrongEmail, password))
				.thenThrow(new BadCredentialsException(""));

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts/login")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(accountRequestDto)));

		// then
		response.andDo(print())
				.andExpect(status().isUnauthorized());
	}

	@Test
	public void authenticate_seccess() throws Exception {
		// given
		String token = "testToken";

		Mockito.when(accountService.autenticate(token))
				.thenReturn(true);

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts/authenticate")
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(token)));

		// then
		response.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void authenticate_wrongToken() throws Exception {
		// given
		String token = "wrongToken";

		Mockito.when(accountService.autenticate(token))
				.thenThrow(new ResponseStatusException(HttpStatus.UNAUTHORIZED, "token is not valid"));

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts/authenticate")
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(token)));

		// then
		response.andDo(print())
				.andExpect(status().isUnauthorized());
	}

	@Test
	public void authenticate_missingToken() throws Exception {
		// given
		AuthenticateRequestDto authenticateRequestDto = AuthenticateRequestDto.builder().build();

		// when
		ResultActions response = mockMvc.perform(
				post("/accounts/authenticate")
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(authenticateRequestDto.getToken())));

		// then
		response.andDo(print())
				.andExpect(status().isUnprocessableEntity());
	}
}
