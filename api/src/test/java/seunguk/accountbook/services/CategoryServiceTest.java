package seunguk.accountbook.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import seunguk.accountbook.models.Category;
import seunguk.accountbook.repositrories.CategoryRepository;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {
	@Mock
	private CategoryRepository categoryRepository;

	@InjectMocks
	private CategoryService categoryService;

	@Test
	public void addCategory_alrealyExist() {
		// given
		String categoryName = "testCategory";
		Category category = Category.builder()
				.name(categoryName)
				.build();

		Mockito.when(categoryRepository.findByName(categoryName)).thenReturn(category);

		// when
		ResponseStatusException excep = assertThrows(ResponseStatusException.class, () -> {
			categoryService.addCategory(categoryName);
		});

		// then
		assertEquals(HttpStatus.CONFLICT.value(), excep.getBody().getStatus());
	}

	@Test
	public void setDefaultCategories_jsonNotExist() {
		assertThrows(NullPointerException.class, () -> {
			categoryService.setDefaultCategories("fileNotExist.json");
		});
	}

	@Test
	public void getAllCategories_success() {
		// given
		Category cat1 = Category.builder().name("cat1").build();
		Category cat2 = Category.builder().name("cat2").build();

		List<Category> categories = Arrays.asList(new Category[] { cat1, cat2 });

		Mockito.when(categoryRepository.findAll()).thenReturn(categories);

		// when
		categoryService.getAllCategories();

		// then
		Mockito.verify(categoryRepository).findAll();
	}
}
