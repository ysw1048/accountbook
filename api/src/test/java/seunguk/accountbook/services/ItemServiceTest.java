package seunguk.accountbook.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import seunguk.accountbook.controllers.dto.Item_editItemDto;
import seunguk.accountbook.controllers.dto.Item_getItemsDto;
import seunguk.accountbook.models.Account;
import seunguk.accountbook.models.Category;
import seunguk.accountbook.models.Item;
import seunguk.accountbook.repositrories.AccountRepository;
import seunguk.accountbook.repositrories.CategoryRepository;
import seunguk.accountbook.repositrories.ItemRepository;
import seunguk.accountbook.services.dto.ItemService_AddItemDto;

@ExtendWith(MockitoExtension.class)
public class ItemServiceTest {
    @Mock
    private ItemRepository itemRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private ItemService itemService;

    @Test
    public void getAllItems_success() {
        // given
        Item item1 = Item.builder().name("item1").build();
        Item item2 = Item.builder().name("item2").build();
        Item item3 = Item.builder().name("item3").build();

        List<Item> expectedItemList = Arrays.asList(new Item[] {item1, item2, item3});
        
        Mockito.when(itemRepository.findAll()).thenReturn(expectedItemList);

        // when
        List<Item> realItemList = itemService.getAllItems();

        // then
        assertEquals(expectedItemList.size(), realItemList.size());
    }

    @Test
    public void getUserItems_sucess() {
        // given
        String accountEmail = "test@test.com";
        Account account = Account.builder()
                        .email(accountEmail)
                        .build();
        Optional<Account> optionalAccount = Optional.of(account);
        Mockito.when(accountRepository.findByEmail(accountEmail)).thenReturn(optionalAccount);

        String categoryName = "category1";
        int categoryId = 1;
        Category category = Category.builder()
                .id(categoryId)
				.name(categoryName)
				.build();
        Optional<Category> optionalCategory = Optional.of(category);            
        Mockito.when(categoryRepository.findById(categoryId)).thenReturn(optionalCategory);

        Item item1 = Item.builder().name("item1").category(category).account(account).build();
        Item item2 = Item.builder().name("item2").category(category).account(account).build();
        Item item3 = Item.builder().name("item3").category(category).account(account).build();
        List<Item> expectedItemList = Arrays.asList(new Item[] {item1, item2, item3});
        Mockito.when(itemRepository.findByAccount(account)).thenReturn(expectedItemList);

        // when
        List<Item_getItemsDto> realItemListDto = itemService.getUserItems(accountEmail);
        
        // then
        assertEquals(expectedItemList.size(), realItemListDto.size());
    }

    @Test
    public void addItem_success() {
        // given
        String name = "item1";
		BigDecimal price = BigDecimal.valueOf(100);
		Integer accountId = 1;
		Integer categoryId = 1;
		Boolean monthly = false;
		Boolean yearly = false;
		Timestamp transactionDate = new Timestamp(System.currentTimeMillis());
        ItemService_AddItemDto addItemDto = ItemService_AddItemDto.builder()
                                        .name(name)
                                        .price(price)
                                        .accountId(accountId)
                                        .categoryId(categoryId)
                                        .monthly(monthly)
                                        .yearly(yearly)
                                        .transactionDate(transactionDate)
                                        .build();

        String accountEmail = "test@test.com";
        Account account = Account.builder()
                        .id(accountId)
                        .email(accountEmail)
                        .build();                        
        Optional<Account> optionalAccount = Optional.of(account);
        Mockito.when(accountRepository.findById(accountId)).thenReturn(optionalAccount);

        String categoryName = "category1";
        Category category = Category.builder()
                .id(categoryId)
				.name(categoryName)
				.build();
        Optional<Category> optionalCategory = Optional.of(category);            
        Mockito.when(categoryRepository.findById(categoryId)).thenReturn(optionalCategory);

        Item item = Item.builder()
                .name(name)
                .price(price)
                .account(account)
                .category(category)
                .monthly(monthly)
                .yearly(yearly)
                .transaction_date(transactionDate)
                .build();
        Mockito.when(itemRepository.save(Mockito.any(Item.class))).thenReturn(item);

        // when
        Item addedItem = itemService.addItem(addItemDto);

        // then
        assertEquals(addItemDto.getName(), addedItem.getName());
        assertEquals(addItemDto.getPrice(), addedItem.getPrice());
        assertEquals(addItemDto.getAccountId(), addedItem.getAccount().getId());
        assertEquals(addItemDto.getCategoryId(), addedItem.getCategory().getId());
        assertEquals(addItemDto.getMonthly(), addedItem.getMonthly());
        assertEquals(addItemDto.getYearly(), addedItem.getYearly());
        assertEquals(addItemDto.getTransactionDate(), addedItem.getTransactionDate());
    }

    @Test
    public void editItem_success() {
        // given
        int itemId = 1;
        String name = "item1";
		BigDecimal price = BigDecimal.valueOf(100);
		Integer accountId = 1;
		Integer categoryId = 1;
		Boolean monthly = false;
		Boolean yearly = false;
		Timestamp transactionDate = new Timestamp(System.currentTimeMillis());
        Item_editItemDto editItemDto = Item_editItemDto.builder()
                                        .id(itemId)
                                        .name(name)
                                        .price(price)
                                        .accountId(accountId)
                                        .categoryId(categoryId)
                                        .monthly(monthly)
                                        .yearly(yearly)
                                        .transactionDate(transactionDate)
                                        .build();

        String accountEmail = "test@test.com";
        Account account = Account.builder()
                        .id(accountId)
                        .email(accountEmail)
                        .build();                        
        Optional<Account> optionalAccount = Optional.of(account);
        Mockito.when(accountRepository.findByEmail(accountEmail)).thenReturn(optionalAccount);

        String categoryName = "category1";
        Category category = Category.builder()
                .id(categoryId)
				.name(categoryName)
				.build();
        Optional<Category> optionalCategory = Optional.of(category);            
        Mockito.when(categoryRepository.findById(categoryId)).thenReturn(optionalCategory);

        Item item = Item.builder()
                .id(itemId)
                .name(name)
                .price(price)
                .account(account)
                .category(category)
                .monthly(monthly)
                .yearly(yearly)
                .transaction_date(transactionDate)
                .build();
        Optional<Item> optionalItem = Optional.of(item);
        Mockito.when(itemRepository.findById(itemId)).thenReturn(optionalItem);
        Mockito.when(itemRepository.save(Mockito.any(Item.class))).thenReturn(item);

        // when
        Item editedItem = itemService.editItem(accountEmail, editItemDto);

        // then
        assertEquals(editItemDto.getName(), editedItem.getName());
        assertEquals(editItemDto.getPrice(), editedItem.getPrice());
        assertEquals(editItemDto.getAccountId(), editedItem.getAccount().getId());
        assertEquals(editItemDto.getCategoryId(), editedItem.getCategory().getId());
        assertEquals(editItemDto.getMonthly(), editedItem.getMonthly());
        assertEquals(editItemDto.getYearly(), editedItem.getYearly());
        assertEquals(editItemDto.getTransactionDate(), editedItem.getTransactionDate());
    }

    @Test
    public void deleteItem_success() {
        // given
        String accountEmail = "test@test.com";
        Account account = Account.builder()
                        .email(accountEmail)
                        .build();
        Optional<Account> optionalAccount = Optional.of(account);
        Mockito.when(accountRepository.findByEmail(accountEmail)).thenReturn(optionalAccount);

        int itemId = 1;
        Item item = Item.builder().id(itemId).name("item1").account(account).build();
        Optional<Item> optionalItem = Optional.of(item);
        Mockito.when(itemRepository.findById(itemId)).thenReturn(optionalItem);

        // when
        itemService.deleteItem(accountEmail, itemId);

        // then
        Mockito.verify(itemRepository, times(1)).deleteById(itemId);
    }
}
