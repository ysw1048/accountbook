package seunguk.accountbook.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import seunguk.accountbook.configurations.JwtService;
import seunguk.accountbook.controllers.dto.AccountResponseDto;
import seunguk.accountbook.models.Account;
import seunguk.accountbook.repositrories.AccountRepository;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
	@Mock
	private AccountRepository accountRepository;

	@InjectMocks
	private AccountService accountService;

	@Mock
	PasswordEncoder passwordEncoder;
	@Mock
	JwtService jwtService;
	@Mock
	AuthenticationManager authManager;
	@Mock
	UserDetailsService userDetailsService;

	@Test
	public void addAccount_success() {
		// given
		String name = "test";
		String email = "test@test.com";
		String password = "password";
		String token = "token";

		Mockito.when(accountRepository.findByEmail(email)).thenReturn(Optional.empty());
		Mockito.when(accountRepository.save(any())).thenReturn(null);
		Mockito.when(passwordEncoder.encode(password)).thenReturn(password);
		Mockito.when(jwtService.generateToken(any())).thenReturn(token);

		// when
		AccountResponseDto dto = accountService.addAccount(name, email, password);

		// then
		assertEquals(email, dto.getEmail());
		assertEquals(name, dto.getName());
		assertEquals(token, dto.getToken());
	}

	@Test
	public void addAccount_accountAlreadyExists() {
		// given
		String name = "test";
		String email = "test@test.com";
		String password = "password";

		Mockito.when(accountRepository.findByEmail(email)).thenReturn(Optional.of(Mockito.mock(Account.class)));

		// when
		HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> {
			accountService.addAccount(name, email, password);
		});
		// then
		assertEquals(HttpStatus.CONFLICT, exception.getStatusCode());
	}

	@Test
	public void login_success() {
		// given
		String email = "test@test.com";
		String password = "password";
		String token = "token";

		Mockito.when(authManager.authenticate(any())).thenReturn(null);
		Mockito.when(accountRepository.findByEmail(email)).thenReturn(Optional.of(Mockito.mock(Account.class)));
		Mockito.when(jwtService.generateToken(any())).thenReturn(token);

		// when
		AccountResponseDto dto = accountService.login(email, password);

		// then
		assertEquals(email, dto.getEmail());
		assertEquals(token, dto.getToken());
	}

	@Test
	public void login_wrongCredential() {
		// given
		String email = "test@test.com";
		String password = "password";

		Mockito.when(authManager.authenticate(any())).thenThrow(new BadCredentialsException(""));

		// when & then
		assertThrows(BadCredentialsException.class, () -> {
			accountService.login(email, password);
		});
	}

	@Test
	public void authenticate_success() {
		// given
		String token = "token";
		String username = "test@test.com";

		Account account = Account.builder().email(username).build();

		Mockito.when(jwtService.extractUsername(token)).thenReturn(username);
		Mockito.when(userDetailsService.loadUserByUsername(username)).thenReturn(account);
		Mockito.when(jwtService.isTokenValide(token, account)).thenReturn(true);

		// when
		boolean isValid = accountService.autenticate(token);

		// then
		assertTrue(isValid);
	}

	@Test
	public void authenticate_inValidToken() {
		// given
		String token = "token";
		String username = "test@test.com";

		Mockito.when(jwtService.extractUsername(token)).thenThrow(new ResponseStatusException(HttpStatus.UNAUTHORIZED));

		// when & then
		assertThrows(ResponseStatusException.class, () -> {
			accountService.autenticate(token);
		});
	}
}
