package seunguk.accountbook;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import seunguk.accountbook.configurations.JwtService;
import seunguk.accountbook.models.Account;
import seunguk.accountbook.models.Role;

@Service
public class TestHelper {
	@MockBean
	private JwtService jwtService;

	@MockBean
	private UserDetailsService userDetailsService;

	public void setToken(String email) {
		// set token
		String token = "token";
		String name = "testuser";
		String password = "testpwd";

		Account account = Account.builder()
				.name(name)
				.email(email)
				.password(password)
				.role(Role.USER)
				.build();

		when(jwtService.extractUsername(anyString())).thenReturn(email);
		when(userDetailsService.loadUserByUsername(email)).thenReturn(account);
		when(jwtService.isTokenValide(token, account)).thenReturn(true);
	}
}
