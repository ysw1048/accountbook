package seunguk.accountbook.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import seunguk.accountbook.models.Category;
import seunguk.accountbook.services.CategoryService;

@RestController
@RequestMapping("/categories")
public class CategoryController {
	@Autowired
	CategoryService categoryService;

	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	public List<Category> getAllCategories() {
		return categoryService.getAllCategories();
	}

	@PutMapping("/default")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void addDefaultCategories() throws IOException {
		categoryService.setDefaultCategories("categories.json");
	}
}
