package seunguk.accountbook.controllers.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.springframework.validation.annotation.Validated;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
@Validated
public class Item_addItemDto {
	@NotEmpty
	private String name;
	@NotNull
	private BigDecimal price;
	@NotNull
	private Integer accountId;
	@NotNull
	private Integer categoryId;
	@NotNull
	private Boolean monthly;
	@NotNull
	private Boolean yearly;
	@NotNull
	private Timestamp transactionDate;
}
