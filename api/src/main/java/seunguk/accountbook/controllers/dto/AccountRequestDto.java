package seunguk.accountbook.controllers.dto;

import org.springframework.validation.annotation.Validated;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class AccountRequestDto {
	@NotNull
	private String name;
	@NotNull
	private String email;
	@NotNull
	private String password;
}