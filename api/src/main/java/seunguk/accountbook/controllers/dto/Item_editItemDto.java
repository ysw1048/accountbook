package seunguk.accountbook.controllers.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Item_editItemDto {
	@NotNull
	private Integer id;
	private String name;
	private BigDecimal price;
	@NotNull
	private Integer accountId;
	private Integer categoryId;
	private Boolean monthly;
	private Boolean yearly;
	private Timestamp transactionDate;
}
