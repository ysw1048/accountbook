package seunguk.accountbook.controllers.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import seunguk.accountbook.models.Item;

class CategoryInfo {
	Integer id;
	String name;

	public CategoryInfo(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

}

@Data
@Builder
@AllArgsConstructor
public class Item_getItemsDto {
	private Integer id;
	private String name;
	private BigDecimal price;
	private Timestamp transaction_date;
	private Integer accountId;
	private CategoryInfo category;
	private Boolean monthly;
	private Boolean yearly;

	public Item_getItemsDto(Item item, Integer categoryId, String categoryName) {
		this.id = item.getId();
		this.name = item.getName();
		this.price = item.getPrice();
		this.transaction_date = item.getTransactionDate();
		this.accountId = item.getAccount().getId();
		this.category = new CategoryInfo(categoryId, categoryName);
		this.monthly = item.getMonthly();
		this.yearly = item.getYearly();
	}

	public Integer getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public Timestamp gettransaction_date() {
		return this.transaction_date;
	}

	public Integer getAccountId() {
		return this.accountId;
	}

	public CategoryInfo getCategory() {
		return this.category;
	}

	public Boolean getMonthly() {
		return this.monthly;
	}

	public Boolean getYearly() {
		return this.yearly;
	}

}
