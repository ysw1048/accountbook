package seunguk.accountbook.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import seunguk.accountbook.controllers.dto.AccountRequestDto;
import seunguk.accountbook.controllers.dto.AccountResponseDto;
import seunguk.accountbook.controllers.dto.AuthenticateRequestDto;
import seunguk.accountbook.controllers.dto.LoginRequestDto;
import seunguk.accountbook.services.AccountService;

@Slf4j
@Controller
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {
	private final AccountService accountService;

	@PostMapping
	public ResponseEntity<AccountResponseDto> addAccount(@Valid @RequestBody AccountRequestDto dto) {
		log.info("Account Creation name: {}, email: {}", dto.getName(), dto.getEmail());
		return new ResponseEntity<AccountResponseDto>(
				accountService.addAccount(dto.getName(), dto.getEmail(), dto.getPassword()),
				HttpStatus.CREATED);
	}

	@PostMapping("/login")
	public ResponseEntity<AccountResponseDto> login(@Valid @RequestBody LoginRequestDto dto) {
		return ResponseEntity.ok(accountService.login(dto.getEmail(), dto.getPassword()));
	}

	@PostMapping("/authenticate")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public void authenticate(@Valid @RequestBody AuthenticateRequestDto dto) {
		accountService.autenticate(dto.getToken());
	}
}
