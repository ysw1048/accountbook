package seunguk.accountbook.controllers;

import java.math.BigDecimal;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import seunguk.accountbook.controllers.dto.Item_addItemDto;
import seunguk.accountbook.controllers.dto.Item_editItemDto;
import seunguk.accountbook.controllers.dto.Item_getItemsDto;
import seunguk.accountbook.services.ItemService;
import seunguk.accountbook.services.dto.ItemService_AddItemDto;

@Slf4j
@RestController
@RequestMapping("/items")
public class ItemController {

	@Autowired
	private ItemService itemService;

	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	public List<Item_getItemsDto> getItems(Principal principal) {
		log.info("{}", principal);
		return itemService.getUserItems(principal.getName());
	}

	@PostMapping()
	@ResponseStatus(HttpStatus.CREATED)
	public void addItem(@RequestBody @Valid Item_addItemDto addItemDto) {
		String name = addItemDto.getName();
		BigDecimal price = addItemDto.getPrice();
		Integer accountId = addItemDto.getAccountId();
		Integer categoryId = addItemDto.getCategoryId();
		Boolean monthly = addItemDto.getMonthly();
		Boolean yearly = addItemDto.getYearly();
		Timestamp transactionDate = addItemDto.getTransactionDate();

		ItemService_AddItemDto itemService_AddItemDto = ItemService_AddItemDto.builder()
							.name(name)
							.price(price)
							.accountId(accountId)
							.categoryId(categoryId)
							.monthly(monthly)
							.yearly(yearly)
							.transactionDate(transactionDate)
							.build();

		itemService.addItem(itemService_AddItemDto);
	}

	@PutMapping()
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void editItem(Principal principal,
			@RequestBody @Valid Item_editItemDto editItemDto) {
		itemService.editItem(principal.getName(), editItemDto);
	}

	@DeleteMapping()
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteItem(Principal principal, @RequestParam int itemId) {
		itemService.deleteItem(principal.getName(), itemId);
	}

}
