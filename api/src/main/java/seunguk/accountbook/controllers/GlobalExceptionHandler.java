package seunguk.accountbook.controllers;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.security.SignatureException;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler
	protected ResponseEntity<Object> handleUnhandledError(Exception exception) {
		Map<String, Object> objectBody = new LinkedHashMap<>();
		objectBody.put("Current Timestamp", new Date());
		objectBody.put("Status", HttpStatus.INTERNAL_SERVER_ERROR);
		objectBody.put("Errors", exception.getMessage());

		return new ResponseEntity<>(objectBody, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ResponseStatusException.class)
	protected ResponseEntity<Object> handleResponseStatusException(ResponseStatusException exception) {
		Map<String, Object> objectBody = new LinkedHashMap<>();
		objectBody.put("Current Timestamp", new Date());
		objectBody.put("Status", exception.getStatusCode());
		objectBody.put("Errors", exception.getReason());

		return new ResponseEntity<>(objectBody, exception.getStatusCode());
	}

	@ExceptionHandler(HttpClientErrorException.class)
	protected ResponseEntity<Object> handleClientError(HttpClientErrorException exception) {
		Map<String, Object> objectBody = new LinkedHashMap<>();
		objectBody.put("Current Timestamp", new Date());
		objectBody.put("Status", exception.getStatusCode());
		objectBody.put("Errors", exception.getStatusText());

		return new ResponseEntity<>(objectBody, exception.getStatusCode());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	protected ResponseEntity<Object> handleValidationErrors(MethodArgumentNotValidException exception) {
		Map<String, Object> objectBody = new LinkedHashMap<>();
		objectBody.put("Current Timestamp", new Date());
		objectBody.put("Status", String.valueOf(HttpStatus.UNPROCESSABLE_ENTITY));

		// Get all errors
		List<String> exceptionalErrors = exception.getBindingResult()
				.getFieldErrors()
				.stream()
				.map(x -> x.getField() + " " + x.getDefaultMessage())
				.collect(Collectors.toList());

		objectBody.put("Errors", exceptionalErrors);

		return new ResponseEntity<>(objectBody, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	protected ResponseEntity<Object> handleReadableErrors(HttpMessageNotReadableException exception) {
		Map<String, Object> objectBody = new LinkedHashMap<>();
		objectBody.put("Current Timestamp", new Date());
		objectBody.put("Status", String.valueOf(HttpStatus.UNPROCESSABLE_ENTITY));
		objectBody.put("Errors", exception.getMessage());

		return new ResponseEntity<>(objectBody, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler(BadCredentialsException.class)
	protected ResponseEntity<Object> handleBadCredentialErrors(BadCredentialsException exception) {
		Map<String, Object> objectBody = new LinkedHashMap<>();
		objectBody.put("Current Timestamp", new Date());
		objectBody.put("Status", String.valueOf(HttpStatus.UNAUTHORIZED));
		objectBody.put("Errors", exception.getMessage());

		return new ResponseEntity<>(objectBody, HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(ExpiredJwtException.class)
	protected ResponseEntity<Object> handleExpiredJwtErrors(ExpiredJwtException exception) {
		Map<String, Object> objectBody = new LinkedHashMap<>();
		objectBody.put("Current Timestamp", new Date());
		objectBody.put("Status", String.valueOf(HttpStatus.UNAUTHORIZED));
		objectBody.put("Errors", exception.getMessage());

		return new ResponseEntity<>(objectBody, HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(SignatureException.class)
	protected ResponseEntity<Object> handleSignatureException(SignatureException exception) {
		Map<String, Object> objectBody = new LinkedHashMap<>();
		objectBody.put("Current Timestamp", new Date());
		objectBody.put("Status", String.valueOf(HttpStatus.UNAUTHORIZED));
		objectBody.put("Errors", exception.getMessage());

		return new ResponseEntity<>(objectBody, HttpStatus.UNAUTHORIZED);
	}

}
