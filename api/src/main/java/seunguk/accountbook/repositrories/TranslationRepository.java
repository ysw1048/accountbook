package seunguk.accountbook.repositrories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import seunguk.accountbook.models.Translation;

public interface TranslationRepository extends JpaRepository<Translation, Integer> {
	@Query(value = "SELECT * FROM translation WHERE en = :#{#tr.en} OR fr = :#{#tr.fr} OR kr = :#{#tr.kr}", nativeQuery = true)
	Translation findOneLangExists(@Param("tr") Translation translation);
}
