package seunguk.accountbook.repositrories;

import org.springframework.data.jpa.repository.JpaRepository;

import seunguk.accountbook.models.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
	Category findByName(String name);
}
