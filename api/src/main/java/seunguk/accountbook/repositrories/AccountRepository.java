package seunguk.accountbook.repositrories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import seunguk.accountbook.models.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {
	Optional<Account> findByEmail(String email);

	Account findByName(String name);
}
