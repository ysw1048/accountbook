package seunguk.accountbook.repositrories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import seunguk.accountbook.models.Account;
import seunguk.accountbook.models.Item;

public interface ItemRepository extends JpaRepository<Item, Integer> {
	List<Item> findByAccount(Account account);
}
