package seunguk.accountbook.models;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Translation {
	@Id
	@GeneratedValue
	private Integer id;

	@Column(unique = true)
	private String en;
	@Column(unique = true)
	private String fr;
	@Column(unique = true)
	private String kr;

	public Translation() {
	}

	public Translation(String en, String fr, String kr) {
		this.en = en;
		this.fr = fr;
		this.kr = kr;
	}

	public Integer getId() {
		return this.id;
	}

	public String getEn() {
		return this.en;
	}

	public void setEn(String en) {
		this.en = en;
	}

	public String getFr() {
		return this.fr;
	}

	public void setFr(String fr) {
		this.fr = fr;
	}

	public String getKr() {
		return this.kr;
	}

	public void setKr(String kr) {
		this.kr = kr;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof Translation)) {
			return false;
		}
		Translation translation = (Translation) o;
		return Objects.equals(en, translation.en)
				&& Objects.equals(fr, translation.fr) && Objects.equals(kr, translation.kr);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, en, fr, kr);
	}

}
