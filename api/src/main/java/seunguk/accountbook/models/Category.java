package seunguk.accountbook.models;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Category {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(nullable = false, unique = true)
	private String name;
	@Column(nullable = false)
	private Boolean isCustom;
	@ManyToOne(optional = true)
	private Account account;

	@OneToOne(optional = true)
	private Translation translation;

	public Category(String name) {
		this.name = name;
		this.isCustom = false;
	}

	public Category(String name, Account account) {
		this.name = name;
		this.account = account;
		this.isCustom = true;
	}

	public Category(String name, Translation translation) {
		this.name = name;
		this.translation = translation;
		this.isCustom = false;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isisCustom() {
		return this.isCustom;
	}

	public Boolean getisCustom() {
		return this.isCustom;
	}

	public void setisCustom(Boolean isCustom) {
		this.isCustom = isCustom;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Translation getTranslation() {
		return this.translation;
	}

	public void setTranslation(Translation translation) {
		this.translation = translation;
	}

	public Category id(Integer id) {
		setId(id);
		return this;
	}

	public Category name(String name) {
		setName(name);
		return this;
	}

	public Category isCustom(Boolean isCustom) {
		setisCustom(isCustom);
		return this;
	}

	public Category account(Account account) {
		setAccount(account);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof Category)) {
			return false;
		}
		Category category = (Category) o;
		return Objects.equals(id, category.id) && Objects.equals(name, category.name)
				&& Objects.equals(isCustom, category.isCustom) && Objects.equals(account, category.account);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, isCustom, account);
	}

	@Override
	public String toString() {
		return "{" +
				" id='" + getId() + "'" +
				", name='" + getName() + "'" +
				", isCustom='" + isisCustom() + "'" +
				", account='" + getAccount() + "'" +
				"}";
	}

}
