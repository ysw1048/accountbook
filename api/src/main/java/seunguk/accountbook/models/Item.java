package seunguk.accountbook.models;

import java.math.BigDecimal;
import java.sql.Timestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Item {

	@Id
	@GeneratedValue
	private Integer id;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private BigDecimal price;
	@Column(nullable = false)
	private Timestamp transaction_date;
	private Timestamp modified_at;

	private Boolean monthly;
	private Boolean yearly;

	public Boolean isMonthly() {
		return this.monthly;
	}

	public Boolean getMonthly() {
		return this.monthly;
	}

	public void setMonthly(Boolean monthly) {
		this.monthly = monthly;
	}

	public Boolean isYearly() {
		return this.yearly;
	}

	public Boolean getYearly() {
		return this.yearly;
	}

	public void setYearly(Boolean yearly) {
		this.yearly = yearly;
	}

	@ManyToOne
	private Account account;
	@ManyToOne
	private Category category;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Timestamp getTransactionDate() {
		return this.transaction_date;
	}

	public void setTransactionDate(Timestamp transaction_date) {
		this.transaction_date = transaction_date;
	}

	public Timestamp getModified_at() {
		return this.modified_at;
	}

	public void setModified_at(Timestamp modified_at) {
		this.modified_at = modified_at;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Item id(Integer id) {
		setId(id);
		return this;
	}

	public Item name(String name) {
		setName(name);
		return this;
	}

	public Item price(BigDecimal price) {
		setPrice(price);
		return this;
	}

	public Item transaction_date(Timestamp transaction_date) {
		setTransactionDate(transaction_date);
		return this;
	}

	public Item modified_at(Timestamp modified_at) {
		setModified_at(modified_at);
		return this;
	}

	public Item account(Account account) {
		setAccount(account);
		return this;
	}

	public Item category(Category category) {
		setCategory(category);
		return this;
	}

}
