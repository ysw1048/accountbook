package seunguk.accountbook.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import seunguk.accountbook.services.dto.CategoryService_CategoriesDto;

public class JsonConverter {
	final private static ObjectMapper mapper = new ObjectMapper();

	public static String serialization(Object obj) throws JsonProcessingException {
		return mapper.writeValueAsString(obj);
	}

	public static Object deserialization(String str) throws JsonProcessingException {
		return mapper.readValue(str, Object.class);
	}

	public static CategoryService_CategoriesDto readCategoriesDto(String categories) throws JsonProcessingException {
		return mapper.readValue(categories, CategoryService_CategoriesDto.class);
	}

}
