package seunguk.accountbook.services;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import seunguk.accountbook.controllers.dto.Item_editItemDto;
import seunguk.accountbook.controllers.dto.Item_getItemsDto;
import seunguk.accountbook.models.Account;
import seunguk.accountbook.models.Category;
import seunguk.accountbook.models.Item;
import seunguk.accountbook.repositrories.AccountRepository;
import seunguk.accountbook.repositrories.CategoryRepository;
import seunguk.accountbook.repositrories.ItemRepository;
import seunguk.accountbook.services.dto.ItemService_AddItemDto;

@Slf4j
@Service
public class ItemService {
	@Autowired
	private ItemRepository itemRepository;
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private CategoryRepository categoryRepository;

	public List<Item> getAllItems() {
		return itemRepository.findAll();
	}

	public List<Item_getItemsDto> getUserItems(String email) {
		try {
			List<Item> items = getItemsByAccount(email);
			List<Item_getItemsDto> itemsDto = new ArrayList<Item_getItemsDto>();
			for (Item item : items) {
				Item_getItemsDto itemDto = wrapItemInDto(item);
				itemsDto.add(itemDto);
			}
			return itemsDto;
		} catch (Exception err) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found", err);
		}
	}

	private List<Item> getItemsByAccount(String accountEmail) {
		Account account = accountRepository.findByEmail(accountEmail).get();
		return itemRepository.findByAccount(account);
	}

	private Item_getItemsDto wrapItemInDto(Item itemToWrap) {
		Category category = categoryRepository.findById(itemToWrap.getCategory().getId()).get();
		return new Item_getItemsDto(itemToWrap, category.getId(), category.getName());
	}

	public Item addItem(ItemService_AddItemDto addItemDto) {
		try {
			String name = addItemDto.getName();
			BigDecimal price = addItemDto.getPrice();
			Timestamp transactionDate = addItemDto.getTransactionDate();
			Account account = accountRepository.findById(addItemDto.getAccountId()).get();
			Category category = categoryRepository.findById(addItemDto.getCategoryId()).get();
	
			boolean monthly = addItemDto.getMonthly();
			boolean yearly = changeYearlyIfMontlyAndYearlyAreTrue(monthly, addItemDto.getYearly());

			Item item = Item.builder()
							.name(name)
							.price(price)
							.transaction_date(transactionDate)
							.account(account)
							.category(category)
							.monthly(monthly)
							.yearly(yearly)
							.build();

			log.info("item creation with name: {}, price: {}, accountId: {}, category: {} at {}", name, price,
					account,
					category, transactionDate);

			return itemRepository.save(item);
		} catch (Exception err) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "account or category not found", err);
		}
	}

	private boolean changeYearlyIfMontlyAndYearlyAreTrue(boolean monthly, boolean yearly) {
		if (monthly && yearly) {
			return false;
		}
		return yearly;
	}


	public Item editItem(String accountEmail, Item_editItemDto dto) {
		try {
			Integer itemId = dto.getId();
			Item item = getAccountByItemId(accountEmail, itemId);
			
			item = setItemNameFromDto(item, dto);
			item = setItemPriceFromDto(item, dto);
			item = setItemCategoryFromDto(item, dto);
			item = setItemMonthlyFromDto(item, dto);
			item = setItemYearlyFromDto(item, dto);
			item = setItemTransactionDateFromDto(item, dto);
			item.setModified_at(new Timestamp(System.currentTimeMillis()));

			return itemRepository.save(item);
		} catch (Exception err) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "account or category not found", err);
		}
	}

	private Item getAccountByItemId(String accountEmail, Integer itemId) {
		Item item = itemRepository.findById(itemId).get();

		checkItemBelongsToRequester(accountEmail, itemId);

		return item;
	}

	private void checkItemBelongsToRequester(String accountEmail, int ItemId) {
		Account account = accountRepository.findByEmail(accountEmail).get();
		Item item = itemRepository.findById(ItemId).get();

		Integer itemAccountId = item.getAccount().getId();
		Integer requesterAccountId = account.getId();

		if (itemAccountId != requesterAccountId)
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You can only delete your own item");

	}

	private Item setItemNameFromDto(Item item, Item_editItemDto dto) {
		if (Objects.nonNull(dto.getName()))
			item.setName(dto.getName());
		return item;
	}
	private Item setItemPriceFromDto(Item item, Item_editItemDto dto) {
		if (Objects.nonNull(dto.getPrice()))
			item.setPrice(dto.getPrice());
		return item;
	}
	private Item setItemCategoryFromDto(Item item, Item_editItemDto dto) {
		if (Objects.nonNull(dto.getCategoryId())) {
			Category category = categoryRepository.findById(dto.getCategoryId()).get();
			item.setCategory(category);
		}
		return item;
	}
	private Item setItemMonthlyFromDto(Item item, Item_editItemDto dto) {
		if (Objects.nonNull(dto.getMonthly()))
				item.setMonthly(dto.getMonthly());
		return item;
	}
	private Item setItemYearlyFromDto(Item item, Item_editItemDto dto) {
		if (Objects.nonNull(dto.getYearly()))
			item.setMonthly(dto.getYearly());
		return item;
	}
	private Item setItemTransactionDateFromDto(Item item, Item_editItemDto dto) {
		if (Objects.nonNull(dto.getTransactionDate()))
			item.setTransactionDate(dto.getTransactionDate());
		return item;
	}

	public void deleteItem(String accountEmail, int itemId) {
		try {
			checkItemBelongsToRequester(accountEmail, itemId);
			itemRepository.deleteById(itemId);
		} catch (Exception err) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "account or category not found");
		}
	}

	
}
