package seunguk.accountbook.services.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class ItemService_AddItemDto {
    String name;
    BigDecimal price;
    Integer accountId;
    Integer categoryId;
    Boolean monthly;
    Boolean yearly;
    Timestamp transactionDate;
}
