package seunguk.accountbook.services.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryService_CategoriesDto {
	@JsonProperty("categories")
	private List<CategoryService_CategoryDto> categories;

	public List<CategoryService_CategoryDto> getCategories() {
		return this.categories;
	}

}
