package seunguk.accountbook.services.dto;

public class CategoryService_CategoryDto {
	private String en;
	private String fr;
	private String kr;

	public String getEn() {
		return this.en;
	}

	public String getFr() {
		return this.fr;
	}

	public String getKr() {
		return this.kr;
	}

}
