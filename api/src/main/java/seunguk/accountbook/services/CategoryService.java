package seunguk.accountbook.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


import lombok.extern.slf4j.Slf4j;
import seunguk.accountbook.models.Category;
import seunguk.accountbook.models.Translation;
import seunguk.accountbook.repositrories.CategoryRepository;
import seunguk.accountbook.repositrories.TranslationRepository;
import seunguk.accountbook.services.dto.CategoryService_CategoriesDto;
import seunguk.accountbook.services.dto.CategoryService_CategoryDto;
import seunguk.accountbook.tools.JsonConverter;

@Slf4j
@Service
public class CategoryService {
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private TranslationRepository translationRepository;

	public void addCategory(String name) {
		Category newCategory = new Category(name);
		Category cat = categoryRepository.findByName(name);

		if (cat == null) {
			categoryRepository.save(newCategory);
		} else
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Category already exists");
	}

	public void setDefaultCategories(String dataFileName) throws IOException {
		String categoriesString = getDefaultCategoriesFromfile(dataFileName);
		log.info("categories file content: {}", categoriesString);
		
		CategoryService_CategoriesDto categories = JsonConverter.readCategoriesDto(categoriesString);

		for (CategoryService_CategoryDto categoryDto : categories.getCategories()) {
			if (checkCategoryAlreadyAdded(categoryDto)) {
				// log.debug("Category {} not exists", category.getName());
				Translation tr = addTranslation(categoryDto.getEn(), categoryDto.getFr(), categoryDto.getKr());
				categoryRepository.save(new Category(categoryDto.getEn(), tr));
			} else {
				log.debug("Category {} exists", categoryDto);
			}
		}
	}

	private String getDefaultCategoriesFromfile(String defaultCategoriesfile) throws IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream(defaultCategoriesfile);
		return new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
	}

	private boolean checkCategoryAlreadyAdded(CategoryService_CategoryDto categoryToCheck) {
		Category category = categoryRepository.findByName(categoryToCheck.getEn());
		return Objects.isNull(category);
	}

	private Translation addTranslation(String en, String fr, String kr) {
		Translation translation = new Translation(en, fr, kr);

		Translation foundTranslation = translationRepository.findOneLangExists(translation);
		if (!Objects.isNull(foundTranslation)) {
			if (!translation.equals(foundTranslation)) {
				foundTranslation.setEn(en);
				foundTranslation.setFr(fr);
				foundTranslation.setKr(kr);
				translationRepository.save(foundTranslation);
			}
			return foundTranslation;
		} else {
			translationRepository.save(translation);
			return translation;
		}
	}

	public List<Category> getAllCategories() {
		return categoryRepository.findAll();
	}

	
}
