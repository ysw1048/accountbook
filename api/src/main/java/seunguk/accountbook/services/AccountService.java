package seunguk.accountbook.services;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import lombok.RequiredArgsConstructor;
import seunguk.accountbook.configurations.JwtService;
import seunguk.accountbook.controllers.dto.AccountResponseDto;
import seunguk.accountbook.models.Account;
import seunguk.accountbook.models.Role;
import seunguk.accountbook.repositrories.AccountRepository;

@Service
@RequiredArgsConstructor
public class AccountService {
	private final AccountRepository accountRepository;
	private final PasswordEncoder passwordEncoder;
	private final JwtService jwtService;
	private final AuthenticationManager authManager;
	private final UserDetailsService userDetailsService;

	public AccountResponseDto addAccount(String name, String email, String password) {
		Account account = Account.builder()
				.name(name)
				.email(email)
				.password(passwordEncoder.encode(password))
				.role(Role.USER)
				.build();

		if (accountRepository.findByEmail(email).isPresent()) {
			throw new HttpClientErrorException(HttpStatus.CONFLICT, "account already exists");
		}
		accountRepository.save(account);
		String jwtToken = jwtService.generateToken(account);
		return AccountResponseDto.builder()
				.email(email)
				.name(name)
				.id(account.getId())
				.token(jwtToken)
				.build();
	}

	public AccountResponseDto login(String email, String password) {
		authManager.authenticate(
				new UsernamePasswordAuthenticationToken(email, password));
		Account account = accountRepository.findByEmail(email)
				.orElseThrow();
		String jwtToken = jwtService.generateToken(account);
		return AccountResponseDto.builder()
				.token(jwtToken)
				.email(email)
				.name(account.getName())
				.id(account.getId())
				.token(jwtToken)
				.build();
	}

	public boolean autenticate(String token) {
		try {
			String username = jwtService.extractUsername(token);
			UserDetails userDetails = userDetailsService.loadUserByUsername(username);
			return jwtService.isTokenValide(token, userDetails);
		} catch (Exception excep) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "token is not valid");
		}

	}
}
