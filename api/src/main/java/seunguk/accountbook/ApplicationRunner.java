package seunguk.accountbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import lombok.extern.slf4j.Slf4j;
import seunguk.accountbook.services.AccountService;
import seunguk.accountbook.services.CategoryService;

@Slf4j
@Component
public class ApplicationRunner implements CommandLineRunner {

	@Autowired
	AccountService accountService;
	@Autowired
	CategoryService categoryService;

	@Value("${ADMIN_NAME}")
	private String ADMIN_NAME;
	@Value("${ADMIN_EMAIL}")
	private String ADMIN_EMAIL;
	@Value("${ADMIN_PASSWORD}")
	private String ADMIN_PASSWORD;

	@Override
	public void run(String... args) throws Exception {
		try {
			accountService.addAccount(ADMIN_NAME, ADMIN_EMAIL, ADMIN_PASSWORD);
		} catch (HttpClientErrorException err) {
			if (err.getStatusCode() == HttpStatus.CONFLICT) {
				log.info("{} already exists, it is normal if it's not the first run of the application", ADMIN_EMAIL);
			}
		}
		try {
			accountService.addAccount("guest", "guest@guest.com", "guest");
		} catch (HttpClientErrorException err) {
			if (err.getStatusCode() == HttpStatus.CONFLICT) {
				log.info("{} already exists, it is normal if it's not the first run of the application",
						"guest@guest.com");
			}
		}
		categoryService.setDefaultCategories("categories.json");
	}
}
