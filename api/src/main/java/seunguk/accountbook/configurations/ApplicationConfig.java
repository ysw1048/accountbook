package seunguk.accountbook.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import lombok.RequiredArgsConstructor;
import seunguk.accountbook.repositrories.AccountRepository;

@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {

	private final AccountRepository accountRepository;

	@Bean
	public UserDetailsService userDetailsService() {
		return username -> accountRepository.findByEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException("User not found"));
		// code below is replaced with a lambda expression which is up here
		/*
		 * return new UserDetailsService() {
		 * 
		 * @Override
		 * public UserDetails loadUserByUsername(String username) throws
		 * UsernameNotFoundException {
		 * return accountRepository.findByEmail(username)
		 * .orElseThrow(() -> new UsernameNotFoundException("User not found"));
		 * }
		 * };
		 */
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
		return config.getAuthenticationManager();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
