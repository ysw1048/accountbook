# Account Book Application
## Introduction

This is a dockerized application for managing account books. It is built using Angular, Spring Boot, and PostgreSQL.

[Link to the deployed application](seunguk.yuns.fr/accountbook/)
[Link to the git](https://gitlab.com/ysw1048/accountbook.git)
## Table of Contents
- [Account Book Application](#account-book-application)
	- [Introduction](#introduction)
	- [Table of Contents](#table-of-contents)
	- [Features](#features)
		- [Month Selection](#month-selection)
		- [Item management](#item-management)
	- [Structure](#structure)
		- [Nginx](#nginx)
		- [Api](#api)
		- [Client](#client)
		- [Datebase](#datebase)
	- [Installation](#installation)
	- [Prerequisites](#prerequisites)
	- [Usage](#usage)
	- [License](#license)
	- [Author](#author)

## Features
### Month Selection
Displaying items on the chosen month

### Item management
An user can add/edit/delete an item with 
- time
- category of the item
- name
- price 

## Structure
### Nginx
It is used for reverse proxy.
It has two URLs
- ```/``` to client
- ```/api``` to api

### Api
```Spring boot``` is used for API server.

- DATA Management with ```JPA```
- Unit Test with ```Junit```
- Http Filter with ```Spring Security```
- Authentication with ```JWT```

Dependencies used for the application
- ```openjdk 20.0.1```
- ```Apache Maven 3.9.2``` 
- ```Spring boot 3.1.1```

### Client
```Angular``` is used for client.

- Store with ```NgRx```

Dependencies used for the application
- ```Node 20.6.1```
- ```Angular 16.2.5```

### Datebase
```PostgresSQL``` is used for DB

You can check the database diagram [./dbdiagram.png](./dbdiagram.png)

![dbdiagram](./dbdiagram.png "DB diagram")

## Installation
If you want to run locally,
Clone the repository: git clone https://gitlab.com/ysw1048/accountbook.git

Set .env file
```
# .env needs such variables
# you can change the db name and user credential
DB_NAME=accountbook
DB_USER=accountbook_user
DB_PASSWORD=accountbook_pwd
DB_PORT=5432

# API_PORT needs to be 8080 for 
API_PORT=8080
WEB_PORT=8081

# Encryption key for JWT token
SECRET_KET=f8f082d88669ebce77c7fe85fac95e7b8b9b7d75727e5cba5092f3fb63424991
# You can customize the credential
ADMIN_NAME=admin
ADMIN_EMAIL=admin@admin.com
# do not use dollar sign for password
ADMIN_PASSWORD=adminpwd

# api version when deploy
API_VERSION=1.0.0

```


in the root of the folder, NB don't forget to run the docker engine
```
docker comepose up -d --build
```
The application will be available on *localhost:8081*

If you want to run the dev mode
```
docker compose -f docker-compose_dev.yml up -d --build
```
The application will be available on *localhost:4200*


## Prerequisites
Docker version 24.0.6

## Usage
API endpoints are listed below
```
/accounts
- POST / 				add account
- POST /login
- POST /authenticate 	check token validity

/categoeis
- GET / 				get all categories
- PUT /default 			set all

/itmes
- GET /					get all items for logged in user
- POST /				add an item
- PUT /					edit an item
- DELETE / 				delete an item
```

At the begging of the application, two users are initiated
- admin user that you defined in .env file
- guest user with id: ```guest```, email: ```guest@guest.com``` and password: ```guest``` 

Default categories are initiated as well.
You can check/change the categories in ```./api/src/main/resources/categories.json```

## License

This project is licensed under the MIT - see the LICENSE file for details.

## Author
Seunguk YUN [Contact](mailto:ysw1048@gmail.com)